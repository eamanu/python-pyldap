Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: pyldap
Source: https://github.com/pyldap/pyldap/

Files: *
Copyright: (c) 2008-2015, pyldap project team
           (c) 2008-2015, Federico Di Gregorio <fog at mixadlive.com>
           (c) 2008-2015, John Benninghoff <johnb at netscape.com>
           (c) 2008-2015, Donn Cave <donn at u.washington.edu>
           (c) 2008-2015, Jason Gunthorpe <jgg at debian.org>
           (c) 2008-2015, gurney_j <gurney_j at 4j.lane.edu>
           (c) 2008-2015, Eric S. Johansson <esj at harvee.billerica.ma.us>
           (c) 2008-2015, David Margrave <davidma at premier1.net>
           (c) 2008-2015, Uche Ogbuji <uche.ogbuji at fourthought.com>
           (c) 2008-2015, Blake Weston <weston at platinum1.cambridge.scr.slb.com>
           (c) 2008-2015, Wido Depping <wido.depping at gmail.com>
           (c) 2008-2015, Deepak Giridharagopal <deepak at arlut.utexas.edu>
           (c) 2008-2015, Ingo Steuwer <steuwer at univention.de>
           (c) 2008-2015, Andreas Hasenack <ahasenack at terra.com.br>
           (c) 2008-2015, Matej Vela <vela at debian.org>
License: PSF-License

Files: debian/*
Copyright: (c) 2016, Thomas Goirand <zigo@debian.org>
License: PSF-License

License: PSF-License
 This LICENSE AGREEMENT is between the Python Software Foundation (“PSF”), and
 the Individual or Organization (“Licensee”) accessing and otherwise using
 Python 2.7.7 software in source or binary form and its associated
 documentation.
 .
 Subject to the terms and conditions of this License Agreement, PSF hereby
 grants Licensee a nonexclusive, royalty-free, world-wide license to reproduce,
 analyze, test, perform and/or display publicly, prepare derivative works,
 distribute, and otherwise use Python 2.7.7 alone or in any derivative version,
 provided, however, that PSF’s License Agreement and PSF’s notice of copyright,
 i.e., “Copyright © 2001-2014 Python Software Foundation; All Rights Reserved”
 are retained in Python 2.7.7 alone or in any derivative version prepared by
 Licensee.
 .
 In the event Licensee prepares a derivative work that is based on or
 incorporates Python 2.7.7 or any part thereof, and wants to make the
 derivative work available to others as provided herein, then Licensee hereby
 agrees to include in any such work a brief summary of the changes made to
 Python 2.7.7.
 .
 PSF is making Python 2.7.7 available to Licensee on an “AS IS” basis. PSF
 MAKES NO REPRESENTATIONS OR WARRANTIES, EXPRESS OR IMPLIED. BY WAY OF
 EXAMPLE, BUT NOT LIMITATION, PSF MAKES NO AND DISCLAIMS ANY REPRESENTATION OR
 WARRANTY OF MERCHANTABILITY OR FITNESS FOR ANY PARTICULAR PURPOSE OR THAT THE
 USE OF PYTHON 2.7.7 WILL NOT INFRINGE ANY THIRD PARTY RIGHTS.
 .
 PSF SHALL NOT BE LIABLE TO LICENSEE OR ANY OTHER USERS OF PYTHON 2.7.7 FOR ANY
 INCIDENTAL, SPECIAL, OR CONSEQUENTIAL DAMAGES OR LOSS AS A RESULT OF
 MODIFYING, DISTRIBUTING, OR OTHERWISE USING PYTHON 2.7.7, OR ANY DERIVATIVE
 THEREOF, EVEN IF ADVISED OF THE POSSIBILITY THEREOF.
 .
 This License Agreement will automatically terminate upon a material breach of
 its terms and conditions.
 .
 Nothing in this License Agreement shall be deemed to create any relationship
 of agency, partnership, or joint venture between PSF and Licensee. This
 License Agreement does not grant permission to use PSF trademarks or trade
 name in a trademark sense to endorse or promote products or services of
 Licensee, or any third party.
 .
 By copying, installing or otherwise using Python 2.7.7, Licensee agrees to be
 bound by the terms and conditions of this License Agreement.
